﻿
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="..\css-js\pages-css.css">
<link rel="stylesheet" type="text/css" href="..\css-js\nav-b.css">
<html>

<head>
    <title>Home</title>
</head>
<body dir="rtl"  class="p1" style="background-image: url('../resources/architecture-building-glass-136413.jpg');">

    <ul>
        <li><a href="sign-up.html">הרשמה</a></li>
        <li><a href="check_s.jsp">כניסה</a></li>


        <li style="float:right"><a href="SplitScreen.jsp">בית</a></li>
        <li style="float:right"><a class="active" href="P1.jsp">טבע</a></li>
        <li style="float:right"><a href="P2.jsp">צ'ק פוינט</a></li>
        <li style="float:right"><a href="P3.jsp">סודהסטרים</a></li>
        <li style="float:right"><a href="P4.jsp">Waze</a></li>
        <li style="float:right"><a href="P5.jsp">גלריה</a></li>
      </ul>
    
<center><h2>התאריך היום הוא: <%= new java.util.Date() %></h2></center>
 <div> 
    <h1>
             טבע תעשיות פרמצבטיות
    </h1>
טבע תעשיות פרמצבטיות בע"מ, המוכרת בשם המקוצר טבע, היא חברת תרופות ישראלית-אמריקאית, החברה המסחרית הגדולה ביותר בישראל. נוסדה ב-1944 על ידי ד"ר גינטר פרידלנדר, אף כי חלקים מסוימים ממה שהפך לימים לחברת טבע, החלו לפעול עוד ב-1901. עיקר התמחותה בתרופות גנריות, החברה הגדולה בעולם בתחום זה, אך לצד זאת, החל מאמצע שנות ה-90 של המאה ה-20, היא מפתחת גם תרופות מקור. יש לה אתרי ייצור, מחקר ושיווק בישראל, באמריקה הצפונית ובאירופה ובסך הכל, פעילות ישירה בכ-60 מדינות בעולם. מכירותיה באמריקה הצפונית ובאירופה תורמות ליותר מ-80% מהכנסותיה. מעסיקה, נכון לסוף 2017, כ-52,000 עובדים מתוכם כ-6,200 בישראל. יו"ר הדירקטוריון הנוכחי הוא סול בארר, והנשיא והמנכ"ל הוא קאר שולץ.


 </div>

    

</body>
</html>