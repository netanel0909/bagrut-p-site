﻿
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="..\css-js\pages-css.css">
<link rel="stylesheet" type="text/css" href="..\css-js\nav-b.css">
<html>

<head>
    <title>Home</title>
</head>
<body dir="rtl"  class="p1" style="background-image: url('../resources/asphalt-automobile-automotive-790176.jpg');">

    <ul>
        <li><a href="sign-up.html">הרשמה</a></li>
        <li><a href="check_s.jsp">כניסה</a></li>


        <li style="float:right"><a href="SplitScreen.jsp">בית</a></li>
        <li style="float:right"><a href="P1.jsp">טבע</a></li>
        <li style="float:right"><a  href="P2.jsp">צ'ק פוינט</a></li>
        <li style="float:right"><a class="active" href="P3.jsp">סודהסטרים</a></li>
        <li style="float:right"><a href="P4.jsp">Waze</a></li>
        <li style="float:right"><a href="P5.jsp">גלריה</a></li>
      </ul>
    <center><h2>התאריך היום הוא: <%= new java.util.Date() %></h2></center>
 <div> 
    <h1>
            סודהסטרים
    </h1>
סודהסטרים אינטרנשיונל בע"מ  היא חברה ישראלית המייצרת מכשירים ביתיים להכנת משקאות מוגזים, וכן סירופים להעשרת הטעם של המשקה. 
     
     החברה פועלת ב-42 מדינות ברחבי העולם, בקרב כ-10 מיליון צרכנים. שווי השוק שלה הגיע במהלך 2011 לרמה של מעל מיליארד דולר.

בישראל מפיצה "סודהסטרים" גם את מכשירי סינון המים "בריטה".

כיום, המכשירים של החברה מיוצרים במפעל ברהט והסירופים במפעל באשקלון.

ב-20 באוגוסט 2018 הודיעה פפסיקו כי היא רוכשת את סודהסטרים ב-3.2 מיליארד דולר. 
</div>
    

</body>
</html>