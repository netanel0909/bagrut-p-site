<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%
    if (session.getAttribute("admin") == null)
    {
        session.setAttribute("admin", "logout");
    }
    if (session.getAttribute("user") == null)
    {
        session.setAttribute("user", "logout");
    }
    if (session.getAttribute("admin").equals("OK"))
    {
        response.sendRedirect("admin-page.jsp"); 
    }
    else
    {
        if (session.getAttribute("user").equals("OK"))
        {
        response.sendRedirect("user-page.jsp"); 
        }
        else
        {
        response.sendRedirect("login-page.html");
        } 
    }
%>